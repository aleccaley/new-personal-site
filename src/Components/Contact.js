import React from 'react'

const contactCss = {
    fontFamily: 'Helvetica',
    fontDisplay: 'swap'
}

class Contact extends React.Component {

    constructor(props){
        super(props)
        this.state = { copyPopupNo: 0 }
        this.copyToClipboard = this.copyToClipboard.bind(this)
    }

    componentDidMount() {
        let el = document.querySelector('.faded');
        el.classList.add('fade-in');
        console.log('contact component mounted')
    }

    copyToClipboard(val, e) {
        console.log('copied')

        // Create an auxiliary hidden input
        var aux = document.createElement("input");
      
        // Get the text from the element passed into the input
        aux.setAttribute("value", val);
      
        // Append the aux input to the body
        document.body.appendChild(aux);
      
        // Highlight the content
        aux.select();
      
        // Execute the copy command
        document.execCommand("copy");
      
        // Remove the input from the body
        document.body.removeChild(aux);


        var tooltip = document.createElement('div');
        var tooltipIdentifier = 'tooltipNo' + this.state.copyPopupNo
        this.setState((prevState) => { return {copyPopupNo: prevState.copyPopupNo + 1}})

        var domString = '<div id='+tooltipIdentifier+' class="tooltipPopup">Copied!</div>';
        tooltip.innerHTML = domString;
        document.body.appendChild(tooltip.firstChild);

        console.log(this.state.copyPopupNo)
        console.log(tooltipIdentifier)

        // var colourString = 'rgb('+(Math.random()*55+199)+','+(Math.random()*55+199)+','+(Math.random()*55+199)+')'

        var positionX = e.pageX + 'px'
        var positionY = e.pageY + 'px'

        // document.getElementById(tooltipIdentifier).style.color = colourString;

        document.getElementById(tooltipIdentifier).style.left = positionX;
        document.getElementById(tooltipIdentifier).style.top = positionY;
      
    }

    render() {  
        return (
            <div className="contact" style={contactCss}>
                <ul className="contactList faded">
                    <li className="copyable" onClick={(e) => this.copyToClipboard('aleccaley@gmail.com', e)}>aleccaley@gmail.com</li>
                    <li>Sydney, NSW</li>
                </ul>
            </div>
        );
    }
}

export default Contact