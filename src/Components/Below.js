import React from 'react'

function Below(){
    let emojiStyle = {
        fontSize: '1.4em',
    }

    return(
        <div className="below">
        {/* <img src={require('./Foto/me_serious.jpg')} alt="A portrait of a hard working young man" className="portrait"/> */}
        <h3>
          Hi!
        </h3>
        <p>
        I'm Alec, a web developer in Sydney. I specialise in Frontend and UX. 
        <br/>Let's make cool stuff together. 
        </p>
        <span role="img" aria-label="Emoji with a big smile" style={emojiStyle}>🐨</span>
      </div>
    )
}

export default Below